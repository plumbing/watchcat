#!/usr/bin/env bash

#source state files
VPN_FILE="/tmp/watchcat.vpn"
NET_FILE="/tmp/watchcat.net"

function f(){
	echo "$1=\"$2\"" > $3
}
function vpn() {
	f "VPN" "$1" "$VPN_FILE" 
}

function net() {
	f "NET" "$1" "$NET_FILE"
}

ping -c 1 192.168.2.1 >/dev/null 2>&1 && NET_IS="UP"

if [[ "$NET_IS" == "UP" ]]; then
	net "UP"
	ping -c 1 10.21.23.5 >/dev/null 2>&1 && VPN_IS="UP"
	if [[ "$VPN_IS" == "UP" ]]; then
		vpn "UP"
	else
        	touch $VPN_FILE
        	source $VPN_FILE
        	case $VPN in
                	1 ) vpn 2 ;;
                	2 ) /etc/init.d/tinc restart& ;;
                	* ) vpn 1 ;;
        	esac
	fi
else
       	touch $NET_FILE
        source $NET_FILE
        case $NET in
                1 ) net 2 ;;
                2 ) /sbin/reboot& ;;
                * ) net 1 ;;
        esac
fi
